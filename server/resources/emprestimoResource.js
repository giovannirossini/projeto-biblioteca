"use strict";
class EmprestimoResource {

    findAll(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.findAll(req.params,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.post(req.body,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    update(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.update(req.body,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.delete(req.body,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = EmprestimoResource;
