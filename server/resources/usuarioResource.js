"use strict";
class UsuarioResource {

    findAll(req, res, next) {
        const UsuarioBusiness = require("../business/usuarioBusiness");
        this.usuarioBusiness = new UsuarioBusiness();
        this.usuarioBusiness.findAll(req.params,
            function callbackUsuario(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const UsuarioBusiness = require("../business/usuarioBusiness");
        this.usuarioBusiness = new UsuarioBusiness();
        this.usuarioBusiness.post(req.body,
            function callbackUsuario(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = UsuarioResource;
