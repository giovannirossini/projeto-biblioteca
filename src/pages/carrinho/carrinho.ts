import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { LivroProvider } from "../../providers/livro";
import { ILivro } from "../../interfaces/ILivro";
import { IReserva } from '../../interfaces/IReserva';
import { LivroDetailsPage } from "../livro-details/livro-details";
import { LivroListPage } from "../livro-list/livro-list";
import { MeusLivrosPage } from '../meus-livros/meus-livros';

@IonicPage()
@Component({
  selector: 'page-carrinho',
  templateUrl: 'carrinho.html',
})
export class CarrinhoPage {
  livro: ILivro;
  items: Array<IReserva>;
  visibilidade: boolean;
  itemsFilter: Array<IReserva>;
  pesquisa: string;

  abrirPesquisa(event) {
    this.visibilidade = true;
}

novoItem(event, item) {
    this.navCtrl.push(LivroListPage, { });
    }

confirmarReserva(event) {
  this.livroProvider.confirmarLivro;
}


pesquisar(event) {
    this.itemsFilter = this.items.filter((i) => {
        if (i.titulo.indexOf(this.pesquisa) >= 0) {
            return true;
        }
        return false;
    })
}

  constructor(public navCtrl: NavController, public navParams: NavParams,  public alertCtrl: AlertController,  public livroProvider: LivroProvider) {
    this.itemsFilter = this.items;
    this.visibilidade = false;
}

itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(LivroDetailsPage, {
        item: item
    });
}

ionViewWillEnter() {
    this.items = this.livroProvider.getReserva();
    this.itemsFilter = this.items;
}


}


