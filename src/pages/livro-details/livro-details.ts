import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ILivro } from "../../interfaces/ILivro";
import { LivroAddPage } from "../livro-add/livro-add";
import { CarrinhoPage } from "../carrinho/carrinho";
import { LivroProvider } from "../../providers/livro";

/**
 * Generated class for the LivroDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-livro-details',
  templateUrl: 'livro-details.html',
})
export class LivroDetailsPage {
  livro: ILivro;
  carrinho: CarrinhoPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public livroProvider: LivroProvider) {
    this.livro = navParams.get('item');
  }
  editarItem(event) {
    this.navCtrl.push(LivroAddPage, {
      item: this.livro
    });
  }
  Reservar(event) {
    let confirmar = this.alertCtrl.create({
      title: 'Reservar',
      message: 'Deseja reservar esse livro?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Reservar',
          handler: () => {
            this.livroProvider.reservarLivro(this.livro);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirmar.present();
  }

  Confirmar(event) {
    let confirmar = this.alertCtrl.create({
      title: 'Confirmar',
      message: 'Confirmar a reservar deste livro?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.livroProvider.confirmarLivro(this.livro);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirmar.present();
  }

  removerItem(event) {
    let confirmar = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja excluir esse registro?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Excluir',
          handler: () => {
            this.livroProvider.removerLivro(this.livro);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirmar.present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LivroDetailsPage');
  }

}
