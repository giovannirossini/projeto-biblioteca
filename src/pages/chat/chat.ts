import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  lista: Observable<any[]>;
  usuario: string;
  mensagem: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db: AngularFireDatabase) {
    this.lista = db.list('/chat').valueChanges();
    //console.log(this.lista);
  }
  enviar() {
    let m = {
      user: this.usuario,
      texto: this.mensagem,
      data: new Date().toJSON()
    };

    let listItems = this.db.list('/chat');
    listItems.push(m).then(() => {
      this.mensagem = "";
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

}
