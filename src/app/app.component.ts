import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FIREBASECONFIG } from './app.module';

import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { LivroListPage } from '../pages/livro-list/livro-list';
import { AutorListPage } from '../pages/autor-list/autor-list';
import { SigninPage } from "../pages/signin/signin";
import { LoginProvider } from '../providers/login';
import firebase from 'firebase';
import { ChatPage } from '../pages/chat/chat';
import { SocialPage } from '../pages/social/social';
import { MeusLivrosPage } from '../pages/meus-livros/meus-livros';
import { CarrinhoPage } from '../pages/carrinho/carrinho';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  signinPage: any = SigninPage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    public loginProvider: LoginProvider) {

    firebase.initializeApp(FIREBASECONFIG);

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.getMenuLogado();
        this.nav.setRoot(this.rootPage);
      }
      else {
        this.getMenuAnonimo();
        this.nav.setRoot(this.signinPage);
      }
    });

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      // { title: 'List', component: ListPage },
      { title: 'Livros', component: LivroListPage },
      { title: 'Autores', component: AutorListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.title == "Sair") {
      this.loginProvider.logout();
      return;
    }
    this.nav.setRoot(page.component);
  }

  getMenuLogado() {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Livros', component: LivroListPage },
      { title: 'Autores', component: AutorListPage },
      { title: 'Reserva', component: CarrinhoPage},
      { title: 'Meus Livros', component: MeusLivrosPage},
      { title: 'Chat', component: ChatPage },
      { title: 'Sobre', component: SocialPage },
      { title: 'Sair', component: null },
    ];
  }
  getMenuAnonimo() {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Sobre', component: SocialPage },
      { title: 'SignIn', component: SigninPage },
    ];
  }
}
