import { Injectable } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ILivro } from "../interfaces/ILivro";
import { IReserva } from "../interfaces/IReserva";

@Injectable()
export class ReservaProvider {
    private livros: ILivro[] = [];
    private res: IReserva[] = [];
    constructor( public alertCtrl: AlertController ) {

    }

getReserva(): IReserva[]{
  return this.res;
}

reservarLivro(livro: IReserva){
  let positionLivro = this.livros.findIndex((l: ILivro) => {
    return l.id == livro.id;
  })

  let positionReserva = this.res.findIndex((l: ILivro) => {
    return l.id == livro.id;
  })

  // Teste para garantir se o livro já está no carrinho
  // if ( livro.id == livro[positionReserva].id )
  //    return livro.id;
  // } else {

  this.livros[positionLivro].qtd = this.livros[positionLivro].qtd -1;
  this.res.push(livro);
}

devolverLivro(livro: ILivro) {
  let position = this.res.findIndex((l: ILivro) => {
      return l.id == livro.id;
  })
  this.livros[position].qtd = livro.qtd +1;
  this.res.splice(position, 1);
}
}
