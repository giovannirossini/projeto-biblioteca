import { Injectable } from '@angular/core';
import { IAutor } from "../interfaces/IAutor";
@Injectable()
export class AutorProvider {
    private autor: IAutor[] = [];
    constructor() {
        if (this.autor.length == 0)
            this.popularAutor();
    }

    popularAutor() {
        //console.log("passei PopularLivros");
        this.autor.push({id : 1, nome : "Mario Quintana", idade : 87, img : "assets/autores/mario.jpg" });
        this.autor.push({id : 2, nome : "Luís de Camões", idade : 56, img : "assets/autores/Camoes3.jpg"  });
        this.autor.push({id : 3, nome : "Machado de Assis", idade : 69, img : "assets/autores/Machado_de_assis.jpg" });
        this.autor.push({id : 4, nome : "Albert Einstein", idade : 76, img : "assets/autores/einstein.jpg" });
        this.autor.push({id : 5, nome : "Gloria Hurtado", idade : 58, img : "assets/autores/gloria.jpg" });
        this.autor.push({id : 6, nome : "Amyr Klink", idade : 62, img : "assets/autores/amyr.jpg" });
        this.autor.push({id : 7, nome : "Nemo Nox", idade : 41, img : "assets/autores/nemo.jpg" });
        this.autor.push({id : 8, nome : "Leon Tolstói", idade : 82, img : "assets/autores/leon-tolstoi-2-l.jpg" });
        this.autor.push({id : 9, nome : "Khalil Gibran", idade : 48, img : "assets/autores/khalil-gibran2.jpg" });
        this.autor.push({id : 10, nome : "Elbert Hubbard", idade : 58, img : "assets/autores/24798-004-78D7A811.jpg" });
    }
    getAutores(): IAutor[] {
        return this.autor;
    }
    adicionarAutor(autor: IAutor) {
        if (autor.id == 0)
            autor.id = this.getNextID();
        this.autor.push(autor);
    }
    removerAutor(autor: IAutor) {
        let position = this.autor.findIndex((a: IAutor) => {
            return a.id == autor.id;
        })
        this.autor.splice(position, 1);
    }
    alterarAutor(autor: IAutor) {
        let position = this.autor.findIndex((a: IAutor) => {
            return a.id == autor.id;
        })
        this.autor[position].nome = autor.nome;
        this.autor[position].idade = autor.idade;
        this.autor[position].img = autor.img;
    }

    getInstancia(): IAutor {
        return {
            id: 0,
            nome: "",
            idade: null,
            img: ""
        };
    }

    private getNextID(): number {
        let nextId = 0;
        if (this.autor.length > 0) {
            nextId = Math.max.apply(
                Math, this.autor.map(function (o) { return o.id; })
            );
        }
        return ++nextId;
    }
}